package com.guoying.common.enums;

/**
 * 操作状态
 * 
 * @author guoying
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
