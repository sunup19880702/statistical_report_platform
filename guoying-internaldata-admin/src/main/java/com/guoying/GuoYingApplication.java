package com.guoying;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 *
 * @author guoying
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class GuoYingApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(GuoYingApplication.class, args);
    }
}
